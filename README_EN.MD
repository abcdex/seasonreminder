SeasonReminder
===

![image](image/image.png)

Reflecting the season to web color, to remind someone of.

## Description
    Spring, there are many leaves sprouting.
    Summer, strong sunlight, plants are all green.
    Autumn, the grass withers and leaves fall.
    Winter, frost covers the ground.

    SeasonReminder reflects the seasonal changes on the web page.

## Demo
    Changing color gradually by date.
![Spring](image/spring.png "Spring")
![Summer](image/summer.png "Summer")
![Autumn](image/autumn.png "Autumn")
![Winter](image/winter.png "Winter")