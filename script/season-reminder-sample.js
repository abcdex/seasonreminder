/**
 * WEBページに季節を反映するサンプル
 */

/**
 * WEBページの全要素を取得し、以下の
 * プロパティに設定されている色を調整する
 * 
 * プロパティ：
 * backgroundColor
 * borderColor
 * color
 */
function initializeSeasonalDesign() {
    var reminder = new SeasonReminder();
    if (!reminder) {
        // SeasonReminderの参照がなければ終了
        return;
    }

    // 背景色は季節を色濃く反映する
    reminder.seasonInfluence = 50;
    reminder.remindAll("backgroundColor");

    // 文字・線の色は季節を淡く反映する
    reminder.seasonInfluence = 10;
    reminder.remindAll("color");
    reminder.remindAll("borderColor");
}

// 季節を反映するロードイベントを追加する
window.addEventListener('load', initializeSeasonalDesign);